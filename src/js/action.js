let getCursor;

function nextTick(fn) {
    setTimeout(fn, 0)
}

export function updateSelf(self){
    if(!getCursor){
        return
    }
    nextTick(() => {
        let oldSelf = getCursor('self')
        if (oldSelf) {
            oldSelf.setIn(['name'], self.name)
        }
    });
}

export function updateClient (client) {
    if(!getCursor){
        return;
    }
    nextTick(() => {
        let oldClient = getCursor('client')
        if(oldClient){
            oldClient.set('name', client.name)
        }
    })
}


export function _init(structure) {
    getCursor = (keyOrPath = []) => structure.cursor([].concat(keyOrPath))
}