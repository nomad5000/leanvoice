import h from 'react-hyperscript'
import component from 'omniscient'

import Client from './LeanvoiceClient'

export default component(
    'Leanvoice',
    ({client,state}) => {
        return Client({client:client,state:state})
    }
)