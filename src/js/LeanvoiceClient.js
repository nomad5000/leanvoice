'use strict'
let component = require('omniscient')
let h = require('react-hyperscript')
import * as action from './action'
import {handleKeyDown} from './utils'

let focusOnEdit = {
    componentDidUpdate(prevProps, prevState) {
        if (this.props.editState.has('title') && !prevProps.editState.has('title')) {
            let node = this.refs.editBox.getDOMNode()
            node.focus()
            node.setSelectionRange(node.value.length, node.value.length)
        }
    }
}


export default component(
  'Client', 
  focusOnEdit,
  ({client, state})=>{
    console.log('rendering Client')
    console.log('client',client)
    console.log('state',state)
    let editing = state.get('editClientMode')
    console.log(editing)
    if(editing){
      console.log('Editing Client')
      let finish = e => {
        action.updateClient(client)
        state.deleteIn(['client','editClientMode'])
      }
      let cancel = _ => editState.delete('title')
      editBox = h('input.edit',{
                      ref: 'editBox',
                      defaultValue: client.get('name'),
                      onBlur: cancel,
                      onChange: e => state.setIn(['client','name'], e.currentTarget.value),
                      onKeyDown: handleKeyDown(finish, cancel)
                  })
    }
    return h('div#client',
    {
      onClick:()=>{
        state.set('editClientMode',true)
      }
    },
    [
      h('.client-detail .name',"test" + client.get('name'))
      /*h('.client-detail .street',client['street']),
      h('.client-detail .postalcode',client['postalcode']),
      h('.client-detail .city',client['city']),*/
    ]
    )
  }
)