'use strict'
import component from 'omniscient'
import react from 'react'
import immstruct from 'immstruct'
import h from 'react-hyperscript'
import Leanvoice from './Leanvoice' 

require("../scss/style.scss")
require('file?name=index.html!../index.html');

let leanvoiceData = immstruct({
  client : {
    /*client data*/
  },
  self: {
    /*name: 'Michael Schmidt',
    street:"Eisenacher Str. 71",
    postalcode:"10823",
    city:'Berlin',
    currency:'€',
    editSelfMode:false,   */
  },
  lines:{},
  state:{}
})

// Init actions
import * as action from './action';
action._init(leanvoiceData);

// Main render cycle
render();
leanvoiceData.on('next-animation-frame', (newStructure, oldStructure, keyPath) => {
  console.log('Subpart of structure swapped.')
  console.log('keyPath:',keyPath)
  console.log('Old structure:', oldStructure.toJSON())
  console.log('New structure:', newStructure.toJSON())
  render();
});

function render() {
    let client = leanvoiceData.cursor('client')
    let state = leanvoiceData.cursor('state')
    console.log(client,state)
    react.render(Leanvoice({client:client, state:state}), document.getElementById('todoapp'));
}

/*

let ClientEditDialog = component('ClientEditDialog', (state)=>{
  console.log('entering client edit mode')
  let client = state.get('client').toJS()
  return h('#client-edit-form',
    [
      h('input#name',{type:'text',placeholder:client['name']}),
      h('input',{type:'text',placeholder:client['street']}),
      h('input',{type:'text',placeholder:client['postalcode']}),
      h('input',{type:'text',placeholder:client['city']}),
      h('input',{type:'button',onClick:()=>{
        console.log($('input#name').val())
      }},'test')
    ]
  )
})


let Self = component('Self', (state)=>{
  let self = state.get('self').toJS()
  let selfKeys = Object.keys(self)
  return h('div#self',[
    h('.self-detail .name',self['name']),
    h('.self-detail .street',self['street']),
    h('.self-detail .postalcode',self['postalcode']),
    h('.self-detail .city',self['city']),
  ])
})

let Invoice = component('Invoice' ,(state)=>{
  return h('div',[Client(state),Self(state)])
})

let render = () => {
  React.render(
    Invoice(state.cursor()),
    document.body
    )
}

render();

state.on('swap', function (newStructure, oldStructure, keyPath) {
  console.log('Subpart of structure swapped.');
  console.log('Old structure:', oldStructure.toJSON());
  console.log('New structure:', newStructure.toJSON());
  render()
});
*/
/*

var Client = component('Client', function ({invoice}){
  var editClient = ()=>invoice.update('editClient',state=>!state);
  return <div className="cleint-data">
    <p className="name">{invoice.get('clientName')}</p>
    <p className="address">{invoice.get('clientAddressStreet')}</p>
    <p className="city">{invoice.get('clientAddressPostalcode') + " " + invoice.get('clientAddressCity')}</p>
    <div className="edit-button" onClick={editClient}>edit</div>
  </div>
}).jsx;

var Self = component('Self', function ({invoice}){
      var editSelf = ()=>invoice.update('editSelf',state=>!state);
      return <div className="self-data">
        <p className="name">{invoice.get('selfName')}</p>
        <p className="address">{invoice.get('selfAddressStreet')}</p>
        <p className="city">{invoice.get('selfAddressPostalcode') + " " + invoice.get('selfAddressCity')}</p>
        <div className="edit-button" onClick={editSelf}>edit</div>
      </div>
}).jsx;

var Invoice = component('Invoice', function(invoice){
  debugger;
  return <div>
    <Client invoice={invoice}/>
    <Self invoice={invoice}/>
  </div>
});



var render = () => {
  React.render(Invoice(invoice.cursor()),document.body);
}
render();
invoice.on('swap', function (newStructure, oldStructure, keyPath) {
  console.log('Subpart of structure swapped.');
  console.log('Old structure:', oldStructure.toJSON());
  console.log('New structure:', newStructure.toJSON());
  render()
});


/*let Invoice = component('Invoice', function (cursor) {
  return d.div({className:'invoice'},Header(cursor),Body(cursor),Footer(cursor))
})


let Header = component('Header', function (cursor) {
  return d.div({className:'header'},Self(cursor),Customer(cursor))
})

let Self = component('Self', (cursor) => {
  return d.div({className:'self-data'},
    d.p({},cursor.get('selfName')),
    d.p({},cursor.get('selfAddressStreet')),
    d.p({},cursor.get('selfAddressPostalcode') + " " +cursor.get('selfAddressCity'))
  )
})

let Customer = component('Customer', function (cursor){
  console.log("self",cursor.get('clientName'))
  return d.div({className:'client-data'},
    d.p({},cursor.get('clientName')),
    d.p({},cursor.get('clientAddressStreet')),
    d.p({},cursor.get('clientAddressPostalcode') + " " +cursor.get('clientAddressCity'))
  )
})

let AddLineButton = component('AddLineButton',(cursor) =>{
  return d.button({
    className:'add-line-button',
    type:'button',
    onClick:(e)=>{
      cursor.update(() => {
        return !cursor.deref()
      })
    },
    },cursor.deref() ? "Cancel": "Add Line")
})

let AddLineDialog = component('AddLineDialog', (cursor) => {
  if(!cursor.get('addLineDialogVisible'))
    return d.div({})
  return d.div({},
    d.input({
      placeholder:'description',
      type:'text',
      onChange:(e)=>{
        cursor.cursor('newLineDescription').update(()=>{return e.currentTarget.value})
      },
      value:cursor.get('newLineDescription')
    }),
    d.input({
      placeholder:'amount',
      type:'number',
      step:'0.01',
      onChange:(e)=>{
        cursor.cursor('newLineAmount').update(()=>{return e.currentTarget.value})
      },
      value:cursor.get('newLineAmount')
    }),
    d.input({
      placeholder:'taxrate',
      type:'number',
      step:'0.01',
      onChange:(e)=>{
        cursor.cursor('newLineTaxRate').update(()=>{return e.currentTarget.value})
      },
      value:cursor.get('newLineTaxRate')
    }),
    d.button({
      onClick:()=>{
        let tmp = cursor.cursor('lines').push(Immutable.Map({ 
          description: cursor.get('newLineDescription'), 
          amount: cursor.get('newLineAmount'),
          taxRate: cursor.get('newLineTaxRate'),
        }))
        console.log("new line",tmp)
        cursor.cursor('newLineDescription').update(()=>{
          return ""
        })
        cursor.cursor('newLineAmount').update(()=>{
          return ""
        })
        cursor.cursor('newLineTaxrate').update(()=>{
          return ""
        })
        cursor.cursor('addLineDialogVisible').update(()=>{return false})
      }
    },"Add Line")
    )
})

let Body = component('Body', (cursor) => {
  let totalWithoutTax = cursor.get('lines').toArray().reduce((prev,curr,index)=>{
    return prev+parseFloat(curr.get('amount'))
  },0)
  let totalWithTax = cursor.get('lines').toArray().reduce((prev,curr,index)=>{
    return prev + curr.get('amount') * (1+curr.get('taxRate'))
  },0)
  let totalTax = totalWithTax - totalWithoutTax
  return (<div className="test">
    test
    </div>);
    
  lines = d.ul({className:"invoice-lines"}, lines.toArray().map((line, index) => {
    return Line(index,line)
  }))
  return d.div({className:"invoice-body"},
    lines,
    AddLineButton(cursor.cursor('addLineDialogVisible')),
    AddLineDialog(cursor),
    d.div({},"Total: " + totalWithoutTax.toFixed(2)),
    d.div({},"Tax: " + totalTax.toFixed(2)),
    d.div({},"Total to pay: " + totalWithTax.toFixed(2))
    )
})

function removeItem(list, i){
  return list.update(currentList => currentList.filter(
    (el,index)=> i !== index;
  ));
}

let Line = component('Line', function (line) {
  return d.li({className:'line'}, 
    d.span({className:'description'},line.get('description')),
    d.span({className:'amount'},line.get('amount') + invoice.cursor('currency')),
    d.span({
      className:'delete',
      onClick: removeItem.bind(null,invoice.cursor('lines'),)
    },'delete')
    )
})

let Footer = component('Footer', (cursor)=>{
  return d.div({},"footer")
})





let render = () => {
  React.render(Invoice(invoice.cursor()),document.body)
}

render()
invoice.on('swap', function (newStructure, oldStructure, keyPath) {
  console.log('Subpart of structure swapped.');
  console.log('Old structure:', oldStructure.toJSON());
  console.log('New structure:', newStructure.toJSON());
  render()
  // e.g. with usage with React
  // React.render(App({ cursor: structure.cursor() }), document.body);
});
*/
/*let test = immstruct({
  foo:[
    {a:'first'},
    {a:'second'}
  ]})

let List = component('List',(cursor)=>{
  return d.div({},cursor.toArray().map((comp,i)=>{
    debugger;
    return Comp(""+i,comp)
  }))
})

let Comp = component('Comp',(cursor)=>{
  return d.div({
    onClick:()=>{
      console.log("cursor",cursor)
      cursor.delete(cursor._keyPath)
    }
  },'delete')
})

React.render(List(test.cursor().get('foo')),document.body)
*/

/*
let Matches = component('Matches', function (cursor) {
  let q = cursor.get('search')
  let libs = cursor.get('libs')
  let matches = libs.filter((lib) => {
    return lib.get('title').indexOf(q) !== -1 || lib.get('url').indexOf(q) !== -1
  })
  return d.ul({}, matches.toArray().map((lib, i) => {
    // Add key through first argument
    return Match(`match-${UUID.create()}`, lib)
  }))
})

let SearchBox = component('SearchBox', function (cursor) {
  return d.div({}, d.input({
    placeholder: 'Search...',
    value: cursor.deref(),
    onChange: function (e) {
      cursor.update(() => {
        return e.currentTarget.value
      })
    },
  }))
})

let AddNameInput = component('AddNameInput',function (cursor){
  console.log("in add input",cursor)
  let isInAddMode = cursor.get('add');
  if(isInAddMode){
    return d.div({},
      d.input({
        placeholder: 'title',
        onChange: function(e){
          cursor.cursor('newTitle').update(()=>{
            return e.currentTarget.value
          })
        },
        value: cursor.get('newTitle')
      }),
      d.input({
        placeholder: 'url',
        onChange: function(e){
          cursor.cursor('newUrl').update(()=>{
            return e.currentTarget.value
          })
        },
        value: cursor.get('newUrl')
      })
    )
  }else{
    return d.div({})
  }
})

let ToggleButton = component('ToggleButton',function (cursor){
  return d.div({
    onClick:function (e) {
      cursor.update(() => {
        return !cursor.deref()
      })
    },
  },cursor.deref() ? 'Cancel' : 'Add')
})

let AddButton = component('AddButton', (cursor) =>{
  let isActive = cursor.get('newTitle') !== "" && cursor.get('newUrl') !== ""
  let isInAddMode = cursor.get('add')
  let cn = isInAddMode ? "" : "hidden"
  cn += isActive ? "" : " inactive"
  return d.div({
    className: cn,
    onClick: function (e) {
      if(!isActive){
        return
      }
      cursor.get('libs').push(Immutable.Map({ title: cursor.get('newTitle'), url: cursor.get('newUrl'), }))
      cursor.cursor('newTitle').update(()=>{
        return ""
      })
      cursor.cursor('newUrl').update(()=>{
        return ""
      })
    }
  },'Add Entry')
})

let MainPanel = component('MainPanel', (cursor) =>{
  return d.div({className:'main-panel'},
    Search(cursor),
    ListEditor(cursor)
    )
})
let Search = component('Search', function (cursor) {
  return d.div({}, SearchBox(cursor.cursor('search')), Matches(cursor))
})

let ListEditor = component('ListEditor',function (cursor){
  return d.div({}, AddNameInput(cursor),AddButton(cursor),ToggleButton(cursor.cursor('add')))
})
*/